FROM alpine:3.5

ENV JAVA_VERSION_MAJOR=8 \
    JAVA_PACKAGE=server-jre \
    JAVA_JCE=standard \
    JAVA_HOME=/opt/jre-1.8 \
    PATH=${PATH}:/opt/jre-1.8/bin \
    GLIBC_VERSION=2.23-r3 \
    LANG=C.UTF-8
    
RUN set -ex && \
    apk upgrade --update && \
    apk add --update libstdc++ curl ca-certificates bash && \
    for pkg in glibc-${GLIBC_VERSION} glibc-bin-${GLIBC_VERSION} glibc-i18n-${GLIBC_VERSION}; do curl -sSL https://github.com/andyshinn/alpine-pkg-glibc/releases/download/${GLIBC_VERSION}/${pkg}.apk -o /tmp/${pkg}.apk; done && \
    apk add --allow-untrusted /tmp/*.apk && \
    rm -v /tmp/*.apk && \
    ( /usr/glibc-compat/bin/localedef --force --inputfile POSIX --charmap UTF-8 C.UTF-8 || true ) && \
    echo "export LANG=C.UTF-8" > /etc/profile.d/locale.sh && \
    /usr/glibc-compat/sbin/ldconfig /lib /usr/glibc-compat/lib
RUN rm -rf /tmp/* /var/cache/apk/*
    
    
RUN mkdir -p /opt/jre-1.8/bin
RUN echo "placeholder" > /opt/jre-1.8/bin/java
RUN chmod 755 /opt/jre-1.8/bin/java

RUN ln -s /opt/jre-1.8/bin/java /usr/bin/java

CMD ["/usr/bin/java"]
