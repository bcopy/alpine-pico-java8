# Pico Docker container for Java 8 (Alpine Linux-based)

## Why

In order to provide the lightest possible containers and reduce maintenance :
* Ship only the bare minimum binaries (to reduce upgrade occurrences)
* Rely on a non-redistributable Java runtime
* Focus on your application

## Recipe

(Inspired from https://github.com/anapsix/docker-alpine-java "sans" the Java Runtime)

* Assemble an Alpine GCC + libstdc++ container (c.f. Dockerfile)
   * Include glibc packages
   * Include CA certificates
   * Create symbolic link to a JRE volume (to be mounted at runtime)

```shell
docker tag <your_image_id> gitlab-registry.cern.ch/ics-swinfra/alpine-pico-java8
```

* You can mount a local JRE to the pico container by using volumes

```shell
docker run -ti --rm -v /usr/lib/jvm/jre-1.8.0:/opt/jre-1.8 gitlab-registry.cern.ch/ics-swinfra/alpine-pico-java8
```

* If the volume is mapped correctly, this should output the Java help instructions.


## Credits

* https://github.com/andyshinn/alpine-pkg-glibc
* https://github.com/anapsix